<?php
namespace Afonya;

use Bitrix\Main\Loader,
    Afonya\HlBlock as HL,
    Afonya\Options,
    Afonya\Sites,
    \Afonya\Users;

class NewsLog
{
    private const MODULE_ID = "afonya.log";
    private const TYPE_EVENT = "STATS_FORM";
    
    public function __construct()
    {
        $this->FIELDS = array(
            "date" => array(), //даты
            "filter" => array(), //фильтр
            "options" => (new Options)->getList(), //опции
            "sites" => (new Sites)->getList(), //сайты
            "users" => array(), //пользователи
            "bestUser" => "", //лучший пользователь
            "actions" => array( //действия
                "add",
                "edit",
                "delete"
            ),
            "stats" => array(
                "total" => array(), //стастикика общая
                "users" => array(), //стастикика по пользователям
            ),
        );

        $this->FIELDS["date"] = $this->getDate($this->FIELDS["options"]);
        $this->FIELDS["filter"] = $this->getFilter($this->FIELDS["options"], $this->FIELDS["date"]);
    }

    /**
     * @param array $arFields
     * @return void
     */
    public function addUpdateEvent(&$arFields): void
    {
        self::eventHandler($arFields);
    }

    /**
     * @param array $arFields
     * @return void
     */
    public function deleteEvent($arFields): void
    {
        self::eventHandler($arFields, "delete");
    }

    /**
     * @param array $arFields
     * @param string $type
     * @return void
     */
    private function eventHandler(array $arFields, string $type = "addUpdate"): void
    {
        $iblockId = (new Options)->getList("MODULE_A_IBLOCK_ID");

        if ($iblockId === null) {
            return;
        }

        if ($arFields["IBLOCK_ID"] === $iblockId) {
            global $USER;

            if ($type === "delete") {
                $action = "delete";
            } else {
                $action = $arFields["~DATE_CREATE"] === "now()" ? "add" : "edit";
            }

            $arResult = array(
                "UF_ACTION" => $action,
                "UF_NEWS_ID"=> $arFields["ID"],
                "UF_USER"   => $USER->GetID(),
                "UF_DATE"   => date("d.m.Y"),
            );

            (new HL)->add($arResult);
        }
    }

    /**
     * @return
    */
    public function sendReport()
    {
        $newsLog = new NewsLog;
        $entity = new HL;

        $arStats = $entity->getList(array(
           "select"     => ["*"],
           "order"      => ["ID" => "ASC"],
           "filter"     => $newsLog->FIELDS["filter"]
        ));

        if ($arStats !== null) {
            foreach ($arStats as $key => $item) {
                $newsLog->FIELDS["stats"]["total"][$item["UF_ACTION"]] += 1;
                $newsLog->FIELDS["stats"]["users"][$item["UF_USER"]][$item["UF_ACTION"]] += 1;
                $newsLog->FIELDS["stats"]["users"][$item["UF_USER"]]["total"] += 1;

                if (!in_array($arStats["UF_USER"], array_keys($newsLog->FIELDS["users"]))) {
                    $newsLog->FIELDS["users"][$item["UF_USER"]] = "";
                }
            }

            if ($newsLog->FIELDS["users"] !== null) {
                $newsLog->FIELDS["users"] = (new Users)->getList($newsLog->FIELDS["users"]);
                $newsLog->FIELDS["bestUser"] = $newsLog->getBestUser($newsLog->FIELDS);
            }

            $arEventFields = [
                "START"         => $newsLog->FIELDS["date"]["start"],
                "END"           => $newsLog->FIELDS["date"]["end"],
                "CREATED"       => $newsLog->FIELDS["stats"]["total"]["add"] ?: 0,
                "EDITED"        => $newsLog->FIELDS["stats"]["total"]["edit"] ?: 0,
                "DELETED"       => $newsLog->FIELDS["stats"]["total"]["delete"] ?: 0,
                "USERS"         => count($newsLog->FIELDS["users"]),
                "BEST_STAFF"    => $newsLog->FIELDS["bestUser"],
                "MORE_STATS"    => $newsLog->getDetailStatsString($newsLog->FIELDS),
            ];

            $newsLog->updateFieldCheck($arStats, $entity);
            $newsLog->sendMessage($newsLog->FIELDS["sites"], $arEventFields);
        }

        return "\Afonya\NewsLog::sendReport();";
    }

    /**
     * @param array $arData
     * @return string
    */
    private function getDetailStatsString(array $arData): string
    {
        $string = "";

        foreach ($arData["stats"]["users"] as $userId => $stat) {
            if ($arData["options"]["MODULE_A_CHECK_STATS"] === "Y") {
                if ($arData["users"][$userId] !== $arData["bestUser"]) {
                    continue; 
                }
            }

            $string .= $arData["users"][$userId] . "\n";

            foreach ($stat as $action => $value) {
                $string .=  GetMessage($action) . ":" . $value . "\n";
            }

            $string .= "\n";
        }

        return $string;
    }

    /**
     * @param array $arData
    */
    private function getBestUser(array $arData)
    {
        $id = null;
        $bestResult = 0;

        foreach ($arData["stats"]["users"] as $userId => $stat) {
            if ($id === null || $bestResult < $stat["total"]) {
               $id = $userId;
               $bestResult = $stat["total"];
            }
        }

        return $arData["users"][$id];
    }

    /**
     * @param array $arData
     * @param $entity
     * @return void
    */
    private function updateFieldCheck(array $arData, $entity): void
    {
        foreach ($arData as $arItem) {
            if ($arItem["UF_CHECK"] !== "Y") {
                $arItem["UF_CHECK"] = "Y";
                $entity->update($arItem);
            }
        }
    }

    /**
     * @param array $arOptions
     * @return array
    */
    private function getDate(array $arOptions): array
    {
        $arDate = [];

        $newDate = new \DateTime();
        $arDate["end"] = $newDate->format("d.m.Y");
        $newDate->modify("-" . $arOptions["MODULE_A_PERIOD"] . " days");
        $arDate["start"] = $newDate->format("d.m.Y");

        return $arDate;
    }

    /**
     * @param array $arOptions
     * @param array $arDate
     * @return array
    */
    private function getFilter(array $arOptions, array $arDate): array
    {
        $arFilter = [
            ">=UF_DATE" => $arDate["start"],
            "<=UF_DATE" => $arDate["end"],
        ];

        if ($arOptions["MODULE_A_CHECK_STATS"] === "Y") {
            $arFilter["UF_CHECK"] = "";
        }

        return $arFilter;
    }

    /**
     * @param array $arSites
     * @param array $arDate
     * @return void
    */
    private function sendMessage(array $arSites, array $arData): void
    {
        \CEvent::Send(self::TYPE_EVENT, $arSites, $arData);
    }
}