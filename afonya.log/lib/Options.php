<?php
namespace Afonya;

use Bitrix\Main\Config\Option;

class Options
{
    private const MODULE_ID = "afonya.log";

    /**
     * @param string $code
    */
    public function getList(string $code = "")
    {
        if ($code) {
            try {
                return Option::get(self::MODULE_ID, $code);
            } catch (Exception $e) {
                return [];
            }
        } else {
            return Option::getForModule(self::MODULE_ID);
        }
    }
}