<?php
    use Bitrix\Main\Loader,
        CModule;

    CModule::IncludeModule("afonya.log");

    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
    IncludeModuleLangFile(__FILE__);

    global $APPLICATION;

    $module_id = $MODULE_ID = basename(__DIR__);
    $RIGHT = $APPLICATION->GetGroupRight($module_id);

    if ($RIGHT >= "R") {
        $bVarsFromForm = false;

        $aTabs = array(
            array(
                "DIV" => "afonya",
                "TAB" => "Настройка модуля",
                "ICON" => "afonya_settings",
                "TITLE" => "Настройка модуля",
                "OPTIONS" => array(
                    "MODULE_A_IBLOCK_ID" => [
                        "ID Инфоблок, который логируется.",
                        ["text", 3]
                    ],
                    "MODULE_A_PERIOD" => [
                        "Количество дней, за которое нужно формировать отчёт.",
                        ["text", 3]
                    ],
                    "MODULE_A_CHECK_STATS" => [
                        "Учитывать в статистике уже отправленные элементы.",
                        ["checkbox", true]
                    ],
                    "MODULE_A_ONLY_BEST" => [
                        "Отправлять отчёт только по лучшему исполнителю.",
                        ["checkbox", true]
                    ],
                ),
            ),
        );
        $tabControl = new CAdminTabControl("tabControl", $aTabs);
        /**
         * @var $REQUEST_METHOD
         * @var $Update
         * @var $Apply
         * @var $RestoreDefaults
         * @var $mid
         */
        if ($REQUEST_METHOD === "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid()) {
            if (strlen($RestoreDefaults) > 0) {
                COption::RemoveOption($module_id);
            } elseif (!$bVarsFromForm) {
                foreach ($aTabs as $i => $aTab) {
                    foreach ($aTab["OPTIONS"] as $name => $arOption) {
                        $disabled = array_key_exists("disabled", $arOption) ? $arOption["disabled"] : "";
                        if ($disabled) {
                            continue;
                        }
                        $val = $_POST[$name];

                        if ($arOption[1][2] == "Y") {
                            $val = serialize($val);
                        }

                        if ($arOption[1][0] === "checkbox" && $val !== "Y") {
                            $val = "N";
                        }
                        COption::SetOptionString($module_id, $name, $val, $arOption[0]);
                    }
                }
            }

            ob_start();
            $Update .= $Apply;
            ob_end_clean();
        }
        $tabControl->Begin();?>

        <form method="post" action="<?php
            echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>" id="options">
            <?php
                foreach ($aTabs as $caTab => $aTab) {
                    $tabControl->BeginNextTab();
                    if ($aTab["DIV"] !== "rights") {
                        foreach ($aTab["OPTIONS"] as $name => $arOption) {
                            if ($bVarsFromForm) {
                                $val = $_POST[$name];
                            } else {
                                $val = COption::GetOptionString($module_id, $name);
                            }

                            $type = $arOption[1];
                            $disabled = array_key_exists("disabled", $arOption) ? $arOption["disabled"] : "";
                            ?>
                            <tr
                                <?php
                                    if (isset($arOption[2]) && strlen(
                                            $arOption[2]
                                        )) {
                                        echo 'style="display:none" class="show-for-' . htmlspecialcharsbx($arOption[2]) . '"';
                                    }
                                ?>
                            >
                                <td width="60%"
                                    <?php if ($type[0] === "textarea") {
                                        echo 'class="adm-detail-valign-top"';
                                    }
                                    ?>
                                >
                                    <label for="<?php echo htmlspecialcharsbx($name) ?>"><?php echo $arOption[0] ?>:</label>
                                <td width="40%">
                                    <?php if ($type[0] === "checkbox") {
                                        ?>
                                        <input type="checkbox" name="<?php echo htmlspecialcharsbx($name) ?>"
                                               id="<?php echo htmlspecialcharsbx($name) ?>" value="Y"<?php if ($val === "Y") {
                                            echo " checked";
                                        } ?><?php if ($disabled) {
                                            echo ' disabled="disabled"';
                                        } ?>><?php if ($disabled) {
                                            echo '<br>' . $disabled;
                                        } ?>
                                        <?php
                                    } elseif ($type[0] === "text") {
                                        ?>
                                        <input type="text" size="<?php echo $type[1] ?>" maxlength="255"
                                               value="<?php echo htmlspecialcharsbx($val) ?>"
                                               name="<?php echo htmlspecialcharsbx($name) ?>">
                                        <?php
                                    } elseif ($type[0] === "textarea") {
                                        ?>
                                        <textarea rows="<?php echo $type[1] ?>" name="<?php echo htmlspecialcharsbx($name) ?>" style=
                                        "width:100%"><?php echo htmlspecialcharsbx($val) ?></textarea>
                                        <?php
                                    } elseif ($type[0] === "select") {
                                        ?>
                                        <?php if (count($type[1])) {
                                            $multy = "";
                                            if ($type[2]) {
                                                $multy = "multiple";
                                                $val = unserialize($val);
                                            }
                                            ?>

                                            <select <?php echo $multy;?> name="<?php echo htmlspecialcharsbx($name) ?><?php if ($multy) {echo "[]";}?>" onchange="doShowAndHide()">
                                                <?php foreach ($type[1] as $key => $value) {?>
                                                    <option value="<?php echo htmlspecialcharsbx($key) ?>"
                                                        <?php if ($multy) {
                                                            if (in_array($key, $val)) {
                                                                echo 'selected="selected"';
                                                            }
                                                        } else {
                                                            if ($val === $key) {
                                                                echo 'selected="selected"';
                                                            }
                                                        }?>
                                                    >
                                                        <?php echo htmlspecialcharsEx($value)?>  
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <?php
                                        } else {
                                            ?>
                                            <?php echo GetMessage("ZERO_ELEMENT_ERROR"); ?>
                                            <?php
                                        } ?>
                                        <?php
                                    } elseif ($type[0] === "note") {
                                        ?>
                                        <?php echo BeginNote(), $type[1], EndNote(); ?>
                                        <?php
                                    } ?>
                                </td>
                            </tr>
                            <?php
                        }
                    } elseif ($aTab["DIV"] === "rights") {
                        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
                    }
                } ?>

            <?php
                $tabControl->Buttons(); ?>
            <input type="submit" name="Update" value="<?= GetMessage("MAIN_SAVE") ?>"
                   title="<?= GetMessage("MAIN_OPT_SAVE_TITLE") ?>" class="adm-btn-save">
            <input type="submit" name="Apply" value="<?= GetMessage("MAIN_OPT_APPLY") ?>"
                   title="<?= GetMessage("MAIN_OPT_APPLY_TITLE") ?>">
            <?php
                if (strlen($_REQUEST["back_url_settings"]) > 0):?>
                    <input type="button" name="Cancel" value="<?= GetMessage("MAIN_OPT_CANCEL") ?>"
                           title="<?= GetMessage("MAIN_OPT_CANCEL_TITLE") ?>" onclick="window.location='<?php
                        echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
                    <input type="hidden" name="back_url_settings"
                           value="<?= htmlspecialcharsbx($_REQUEST["back_url_settings"]) ?>">
                <?php
                endif ?>
            <input type="submit" name="RestoreDefaults" title="<?php
                echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>" OnClick="return confirm('<?php
                echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')" value="<?php
                echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
            <?= bitrix_sessid_post() ?>
            <?php
                $tabControl->End(); ?>
        </form>
        <?php
    } ?>