<?php

IncludeModuleLangFile(__FILE__);

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

class afonya_log extends CModule
{
    private const ENTITY = "Stats";
    private const MODULE_ID = 'afonya.log';
    var $MODULE_ID = 'afonya.log';

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . "/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = "afonya.log";
        $this->MODULE_DESCRIPTION = "";
        $this->PARTNER_NAME = "";
        $this->PARTNER_URI = "";
    }

    function DoInstall()
    {
        RegisterModule(self::MODULE_ID);
        $this->InstallAgents();
        $this->InstallDB();
        return true;
    }

    function DoUninstall()
    {
        UnRegisterModule(self::MODULE_ID);
        $this->UnInstallAgents();
        $this->UnInstallDB();
        return true;
    }

    function InstallDB($arParams = array())
    {
        //Регистрация событий
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler("iblock", "OnAfterIBlockElementAdd", self::MODULE_ID, '\Afonya\NewsLog', "addUpdateEvent");
        $eventManager->registerEventHandler("iblock", "OnAfterIBlockElementUpdate", self::MODULE_ID, '\Afonya\NewsLog', "addUpdateEvent");
        $eventManager->registerEventHandler("iblock", "OnAfterIBlockElementDelete", self::MODULE_ID, '\Afonya\NewsLog', "deleteEvent");

        //Создание HL
        if (Loader::IncludeModule("highloadblock")) {
            $this->highBlockAdd(
                "Stats",
                "stats",
                [
                    "ru" => "Статистика",
                    "en" => "Stats",
                ],
                [
                    [
                        'FIELD_NAME' => 'UF_ACTION',
                        'USER_TYPE_ID' => 'string',
                        'XML_ID' => '',
                        'SORT' => 100,
                        'MULTIPLE' => 'N',
                        'MANDATORY' => 'Y',
                        'EDIT_FORM_LABEL' => [
                            'ru' => 'Действие',
                            'en' => 'Action',
                        ],
                        'LIST_COLUMN_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ],
                        'LIST_FILTER_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ]
                    ],
                    [
                        'FIELD_NAME' => 'UF_NEWS_ID',
                        'USER_TYPE_ID' => 'double',
                        'XML_ID' => '',
                        'SORT' => 100,
                        'MULTIPLE' => 'N',
                        'MANDATORY' => 'Y',
                        'EDIT_FORM_LABEL' => [
                            'ru' => 'ID элемента новости',
                            'en' => 'ID news element',
                        ],
                        'LIST_COLUMN_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ],
                        'LIST_FILTER_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ]
                    ],
                    [
                        'FIELD_NAME' => 'UF_USER',
                        'USER_TYPE_ID' => 'double',
                        'XML_ID' => '',
                        'SORT' => 100,
                        'MULTIPLE' => 'N',
                        'MANDATORY' => 'Y',
                        'EDIT_FORM_LABEL' => [
                            'ru' => 'Пользователь',
                            'en' => 'User',
                        ],
                        'LIST_COLUMN_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ],
                        'LIST_FILTER_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ]
                    ],
                    [
                        'FIELD_NAME' => 'UF_DATE',
                        'USER_TYPE_ID' => 'date',
                        'XML_ID' => '',
                        'SORT' => 100,
                        'MULTIPLE' => 'N',
                        'MANDATORY' => 'Y',
                        'EDIT_FORM_LABEL' => [
                            'ru' => 'Дата',
                            'en' => 'Date',
                        ],
                        'LIST_COLUMN_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ],
                        'LIST_FILTER_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ]
                    ],
                    [
                        'FIELD_NAME' => 'UF_CHECK',
                        'USER_TYPE_ID' => 'string',
                        'XML_ID' => '',
                        'SORT' => 100,
                        'MULTIPLE' => 'N',
                        'MANDATORY' => 'N',
                        'EDIT_FORM_LABEL' => [
                            'ru' => 'Запись уже отправлялась в письме',
                            'en' => 'The entry has already been sent in an email.',
                        ],
                        'LIST_COLUMN_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ],
                        'LIST_FILTER_LABEL' => [
                            'ru' => '',
                            'en' => '',
                        ]
                    ],
                ]
            );
        }

        //Создание опций
        COption::SetOptionString(self::MODULE_ID, "MODULE_A_PERIOD", 7);
        COption::SetOptionString(self::MODULE_ID, "MODULE_A_CHECK_STATS", "Y");
        COption::SetOptionString(self::MODULE_ID, "MODULE_A_ONLY_BEST", "Y");

        //Получение списка сайтов
        $siteIds = [];
        $rsSites = \CSite::GetList($by="sort", $order="desc", []);

        while ($arSite = $rsSites->Fetch()) {
            $siteIds[] = $arSite["LID"];
        }

        if (!$siteIds) {
            $siteIds = ["s1"];
        }

        //Создание почтового события
        $arrCeventTypes[] = [
            "LID"           => SITE_ID,
            "EVENT_NAME"    => "STATS_FORM",
            "NAME"          => "Отправка данных о статистики",
            "DESCRIPTION"   => "
#START# - дата начала сбора статистики
#END# - конечная дата сбора статистики
#CREATED# - создано записей
#EDITED# - отредактировано записей
#DELETED# - удалено записей
#USERS# - количество пользователей участвующих в процессе
#MORE_STATS# - подробная статистика по пользователям
#BEST_STAFF# - лучший сотрудник            
            ",
        ];

        if ($arrCeventTypes) {
            $arrCeventRes = [];
            $et = new \CEventType;

            foreach ($arrCeventTypes as $arrCeventType) {
                $rsEt = $et->Add($arrCeventType);

                if (!$rsEt) {
                    echo $rsEt->LAST_ERROR;
                } else {
                    $arrCeventRes[$rsEt] = $arrCeventType["EVENT_NAME"];
                }
            }
        }

        //Создание почтового шаблона
        $arrCeventTemplates = [
            "STATS_FORM" => [
                "ACTIVE"        => "Y",
                "EVENT_NAME"    => "STATS_FORM",
                "LID"           => $siteIds,
                "EMAIL_FROM"    => "#DEFAULT_EMAIL_FROM#",
                "EMAIL_TO"      => "#DEFAULT_EMAIL_FROM#",
                "SUBJECT"       => "Отправка данных о статистики",
                "BODY_TYPE"     => "html",
                "MESSAGE"       => "
Собрана статистика за период с #START# по #END#.<br/><br/>

Создано: #CREATED#<br/>
Отредактировано: #EDITED#<br/>
Удалено: #DELETED#<br/>
Пользователей участвовало: #USERS#<br/><br/>

Лучший сотрудник<br/>
#BEST_STAFF#<br/><br/>

Подробнее<br/>
#MORE_STATS#
                ",
            ],
        ];

        if ($arrCeventRes) {
            $em = new \CEventMessage;

            foreach ($arrCeventRes as $cEventTypeName) {
                $rsEm = $em->Add($arrCeventTemplates[$cEventTypeName]);

                if (!$res_em){
                    echo $rsEm->LAST_ERROR;
                }    
            }
        }

        return true;
    }

    function UnInstallDB($arParams = array())
    {
        //Удаление событий
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler("iblock", "OnAfterIBlockElementAdd", self::MODULE_ID, '\Afonya\NewsLog', "addUpdateEvent");
        $eventManager->unRegisterEventHandler("iblock", "OnAfterIBlockElementUpdate", self::MODULE_ID, '\Afonya\NewsLog', "addUpdateEvent");
        $eventManager->unRegisterEventHandler("iblock", "OnAfterIBlockElementDelete", self::MODULE_ID, '\Afonya\NewsLog', "deleteEvent");

        //Удаление HL
        if (Loader::IncludeModule("highloadblock")) {
            $rs = HL\HighloadBlockTable::getList(
                array(
                    'filter' => array(
                        'NAME' => self::ENTITY
                    )
                )
            );

            if ($hlblockId = $rs->fetch()) {
                HL\HighloadBlockTable::delete($hlblockId["ID"]); 
            }
        }

        //Удаление опций
        COption::RemoveOption(self::MODULE_ID, "MODULE_A_IBLOCK_ID");
        COption::RemoveOption(self::MODULE_ID, "MODULE_A_PERIOD");
        COption::RemoveOption(self::MODULE_ID, "MODULE_A_CHECK_STATS");
        COption::RemoveOption(self::MODULE_ID, "MODULE_A_ONLY_BEST");

        //Удаление почтового события
        $et = new \CEventType;
        $et->Delete("STATS_FORM");

        //Удаление почтового шаблона
        $rsEm = \CEventMessage::GetList($by="site_id", $order="desc", ["TYPE_ID" => ["STATS_FORM"]]);

        if ($arEm = $rsEm->Fetch()) {
            $em = new \CEventMessage;
            $em->Delete($arEm["ID"]);
        }

        return true;
    }

    function InstallAgents()
    {
        $date = date("d.m.Y H:m");
        date_modify($date, "+1 hour");

        \CAgent::AddAgent(
            "\Afonya\NewsLog::sendReport();",
            self::MODULE_ID,
            "N",
            604800,
            $date,
            "Y",
            $date,
        30);
    }

    function UnInstallAgents()
    {
        \CAgent::RemoveAgent("\Afonya\NewsLog::sendReport();", self::MODULE_ID);

        return true;
    }

    /**
     * @param $name // название hl-блока
     * @param $tableName // таблица hl-блока
     * @param $lang // массив с языковыми переводами названия hl-блока
     * @param $fields // массив полей сущности
     * @param $filling // наполнение полей сущности
     * @return bool
     */
    public function highBlockAdd($name, $tableName, $lang, $fields, $filling = []): bool
    {
        try {
            $res = HL\HighloadBlockTable::getList([     
                "filter" => [
                    "NAME"          => $name,
                    "TABLE_NAME"    => $tableName
                ]
            ]);
        } catch (Exception $e) {
            return false;
        }

        if (!$res->fetch()) {
            try {
                $result = HL\HighloadBlockTable::add([
                    "NAME"          => $name,
                    "TABLE_NAME"    => $tableName
                ]);
            } catch (Exception $e) {
                return false;
            }

            if (!$result->isSuccess()) {
                return false;
            }

            $id = $result->getId();

            foreach ($lang as $key => $value) {
                try {
                    HL\HighloadBlockLangTable::add([
                        "ID"    => $id,
                        "LID"   => $key,
                        "NAME"  => $value
                    ]);
                } catch (Exception $e) {
                    return false;
                }
            }

            $userField = new \CUserTypeEntity;
            $sort = 100;
            $fieldId = null;

            foreach ($fields as $field) {
                $field["ENTITY_ID"] = "HLBLOCK_" . $id;

                $res = \CUserTypeEntity::getList(
                    [],
                    [
                        "ENTITY_ID"     => $field["ENTITY_ID"],
                        "FIELD_NAME"    => $field["FIELD_NAME"]
                    ]
                );

                if (!$res->Fetch()) {
                    $field["SORT"] = $sort;
                    $fieldId = $userField->Add($field);
                    $sort += 100;
                }

                if ($fieldId && isset($filling[$field["FIELD_NAME"]])) {
                    (new \CUserFieldEnum)->SetEnumValues($fieldId, $filling[$field["FIELD_NAME"]]);
                }
            }
        }

        return true;
    }
}